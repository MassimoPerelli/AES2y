clc
clear 
close all
format shortg

%% DATI
p_pompa = 21*10^6; % [Pa]
xmax_mm = 600; %[mm]
xmax = 0.600; %[m]
t = 30; %[s]
B = 1800 * 10^6; %[Pa]
gamma = 1.4; %Cp/Cv azoto

Fmax = 12000 + 40*xmax_mm; %[N]
p0 = (6:20)*10^6; % [Pa] pressioni precarica.

%% CALCOLI

A = 2 .* Fmax ./ p0; %[m]
ds = sqrt(4*A/(3*pi)); %[m]
Dc = 2*ds;%[m]

Volio = A * xmax; %[m^3]
Volio_lt = Volio * 10^3; %[lt]

V0 = Volio./(1-(p0/p_pompa).^(1/gamma)); %[m^3]
V0_lt = V0 * 10^3; %[lt]
Vtot = Volio_lt + V0_lt; %[lt]
Vtot2 = 2 * Volio_lt + V0_lt; %[lt]

Q = Volio ./ t;
W = Q * p_pompa; %[W]
dp = Fmax ./ A;
dx = xmax .* dp ./ B; %[m];
% Il criterio di scelta potrebbe essere Vtot minimo oppure Vtot2 minimo
%% STAMPA A VIDEO




