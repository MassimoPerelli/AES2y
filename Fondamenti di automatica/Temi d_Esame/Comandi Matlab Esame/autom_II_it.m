close all
clear all

s = tf('s');

figure
for mu = [1:5]
    L = mu/(1+10*s)^4;
    nyquist(L)
    grid on
    hold on
end



%%

figure
s = tf('s');
L_1 = (100/s)*(1-s)/((1+10*s)*(1+s))
bode(L_1)
grid on
hold on

L = 0.3*(1-s)/(s*(1+s)*(1+0.1*s))
bode(L)


%%
z = tf('z');
G = 12 / (z^2+3*z+2);
step(G)
grid on


%%
close all
clear all
s = tf('s');
L = (s+3)/((s+1)*(s+2)^2)
figure
rlocus(L)
figure
rlocus(-L)


%%
s = tf('s');
rho = 8;
L = rho*(s-1)/((s+2)*(s-2)^2);
G = L/(1+L);
[z,p,k] = zpkdata(G,'v')

pzmap(G)
