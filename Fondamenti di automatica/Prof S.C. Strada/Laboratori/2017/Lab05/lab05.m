close all

A = [-0.0558 -0.9968 0.0802 0.0415;
      0.598  -0.115 -0.0318    0  ;
      -3.05   0.388  -0.465    0  ;
      0      0.0805     1      0  ];
  
B = [0.0729 0.0001;
     -4.75   1.23;
      1.53  10.63;
        0      0 ];

C = [0 1 0 0;
     0 0 0 1];
 
D = 0;

%% 1
b747 = ss(A,B,C,D);

%% 2
damp(b747)
figure
pzmap(b747)

%% 3
figure
impulse(b747,20) %output 1: yaw rate, output 2: roll rate, 
grid on

%% 4
b747rr = b747(1,1);

%% 5
figure
rlocus(-b747rr) % meno poichè il sistema ha guadagno negativo
sgrid

%% 6 Proportional Controller Design
[k,p]=rlocfind(-b747rr) % meno poichè il sistema ha guadagno negativo
% smorzamento massimizzato nel punto di tangenza tra una retta passante per
% l'origine ed il luogo descritto da uno dei poli complessi (che chiaramente descrivono gli stessi luoghi)
% ricavo:  k = 0.279984404370667

%% 7
clprop = feedback(b747,-k,1,1)
figure
impulse(b747,clprop,20)
title('Proportional Controller Design')

%% 8 Derivative Controller Design
p = 0.33;
s = tf('s');
H = k*s/(s+p);
L = H*b747rr;
figure
rlocus(-L) % meno poichè il sistema ha guadagno negativo
sgrid
[k,p]=rlocfind(-L)

%% 9
clwash=feedback(b747,-k*H ,1,1)
figure
impulse(b747,clwash,20)
title('Derivative Controller Design')
