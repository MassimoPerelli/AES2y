function [dim, tempi] = es2(nmin,nmax,step)

% function [dim, tempi] = es2(nmin,nmax,step)
%
% Rappresenta al variare della dimensionalità il tempo (CPU) di
% calcolo di un prodotto matrice quadrata x vettore. 
% La dimensionalità varia da nmin a nmax con incremento step. 
% Se passo solo due valori (nmin e nmax), l'incremento è uguale a 100. 
%
% Output:
% dim = vettore delle dimensioni utilizzate nel calcolo
% tempi = vettore dei tempi (effettivi) necessari al calcolo

if (nargin == 2)
    incr = 100;
elseif (nargin == 3)
    incr = step;
end
A = rand(nmax,nmax);
v = rand(nmax,1);
dim =[];
tempi = [];
for k = nmin:incr:nmax
    AA = A(1:k,1:k);
    vv = v(1:k,1);
    tic;
    b = AA*vv;
    tc = toc;
    dim = [dim; k];
    tempi = [tempi; tc];
end
plot(dim, tempi(:,1), 'o--')
legend('CPU TIME','Location','NorthWest')
