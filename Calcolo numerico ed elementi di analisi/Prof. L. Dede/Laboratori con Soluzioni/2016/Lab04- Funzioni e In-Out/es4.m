% Laboratorio 4 Esercizio 4
% Scrittura su file, errore assoluto e relativo
% Realizza una tabella di coi valori del fattoriale, della sua
% approssimazione tramite funzione di Stirling e errori compiuti (assoluto
% e relativo)
%
N = input('Inserire N (non troppo grande ...): ');
e = exp(1);
file = fopen('es4.out', 'w');
fprintf(file, '------------------------------------------------------------------\n');
fprintf(file, 'n\t\t n!\t\t S(n)\t Ea\t\t Er\n');
fprintf(file, '------------------------------------------------------------------\n');
nfact = 1;
for n = 1:N
    nfact = n*nfact;
    s = sqrt(2*pi*n)*((n/e)^n);
    Ea = abs(nfact - s);
    Er = Ea/nfact;
    fprintf(file, '%2d\t %9d\t %10.2f\t %3.2e\t %3.2e\n',...
            n, nfact, s, Ea, Er);
end
fclose(f1);
