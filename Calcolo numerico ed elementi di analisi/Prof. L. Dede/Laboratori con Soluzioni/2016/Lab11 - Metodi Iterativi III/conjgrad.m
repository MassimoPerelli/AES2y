function [x,k] = conjgrad(A,b,x0,toll,nmax)
  
% Metodo del gradiente coniugato 
%
% Parametri di ingresso:
%
% A: matrice di sistema
% b: vettore termine noto
% x0: guess iniziale
% tol:  tolleranza criterio d'arresto
% nmax: numero massimo di iterazioni ammesse
% 
% Parametri di uscita:
%
% x: soluzione
% k: numero di iterazioni
%

  n = length(b);
  if ((size(A,1) ~= n) || (size(A,2) ~= n) || (length(x0) ~= n))
    error('Dimensioni incompatibili')
  end
 
  k = 0;
  bnrm2 = norm(b);
  x = x0;
  r = b - A*x0;
  p = r;
  err = 1;
  
  while ((err>toll) && (k<nmax))
    k = k+1;
    q = A*p;
    alpha = (p'*r)/(p'*q);
    x = x + alpha*p;
    r = r - alpha*q;
    beta = (q'*r)/(q'*p);
    p = r - beta*p; 
    err = norm(r)/bnrm2;
  end
  
if (err < toll)
     fprintf('Gradiente coniugato converge in %d iterazioni \n', k);
else
     fprintf('Gradiente coniugato non converge in %d iterazioni. \n', k)
end