%% Esercizio 1

clc, clear all, close all
a = [1 0 0]';
b = [0 23/12 -16/12 5/12]';
stabreg(a,b)

hold on
%stabreg([1], [0 1]) %Regione di stabilità di Eulero esplicito
%stabreg([1], [1 0]) %Regione di stabilità di Eulero implicito
title('Regione di assoluta stabilità per  AB3');
axis equal
grid

fun  = @(t,y) y./(1+t) - y.^2;
% dfun = @(t,y) 1./(1+t) - 2*y; 
fex = @(t) 2*(1+t)./(t.^2 + 2*t +2);

tplot = linspace(0,100,1000);

err = [];
H = [0.1 0.05 0.025 0.0125];
for h = H
    t0 = [0 h 2*h]';
    u0 = fex(t0);
    [t,u] = multistep (a, b, 100, t0, u0, h, fun);
    err = [err max(abs(u-fex(t)))]; 
    if h==0.1
        figure
        plot(t,u,'-r',tplot,fex(tplot),'--k') % plotto la solutione per h=0.1
        legend('Sol numerica','Sol esatta')
        grid on
    end
end
figure
loglog(H,err,'ro--',H,H.^3)
grid
legend('e_h','h^3')


% %% Esercizio 2
% clc, close all, clear all
% [t,u] = ode45('mms',[0 100],[2 0]);
% [tf,uf] = ode45('mms_forz',[0 100],[2 0]);
% plot(t,u(:,1),'b',tf,uf(:,1),'r')
% legend('Oscillazione smorzata','Con forzante esterna')
