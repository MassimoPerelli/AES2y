clear all
close all

run('config.m');

Y0 = [0 0 deg2rad(85) 0 0 0 settings.m0 settings.Iyyf]; % Initial Conditions
    %[x z    theta    u w q      m            Iy     ]


[T,Y] = ode45(@missile, settings.time, Y0, settings.options, settings);

% PLOTS
figure
plot(T,Y(:,2), 'b', 'LineWidth', 2)
title('Altitude Profile')
grid on

figure
plot(T,Y(:,4), 'b', 'LineWidth', 2)
hold on
grid on
plot(T,Y(:,5), 'k', 'LineWidth', 2)
title('Velocity')
legend('u','v')