%% EX.1

A = [50 1 3; 1 6 0; 3 0 1];
B = [50 1 10; 3 20 1; 10 4 70];
C = [7 8 9; 5 4 3; 1 2 6];

%% 1.1.1
% Si verifichi, utilizzando Matlab  o Octave, se le matrici A, B e C
% soddisfano le condizioni sufficienti ed eventualmente necessarie e
% sufficienti per l?esistenza della fattorizzazione LU.


n = length(diag(A));



tens = zeros(n,n,n);




isDom_row = 1;
isDom_column = 1;

for i = 1:n
    
    a_ii = A(i,i);
    
    sum_row = sum(A(i,:));
    if (abs(a_ii) <= abs((sum_row-a_ii)))
     isDom_row = 0;
    end
    
    sum_column = sum(A(:,i));
    if (abs(a_ii) <= abs((sum_column-a_ii)))
        isDom_column = 0;
    end
    
end

if (isDom_row)
    disp('Dominanza diagonale stretta per righe')
else
    disp('NO Dominanza diagonale stretta per righe')
    
end

if (isDom_column)
    disp('Dominanza diagonale stretta per colonne')
else
    disp('NO Dominanza diagonale stretta per colonne')
    
end

%% 1.1.2

if A == A'
    
    lambdas = eig(A);
    
    for i = 1:n
        
        if lambdas(i) < 0
            disp('NO SDP')
            break
        end
        
        if (i==n)
            disp('Matrice SDP')
        end
        
    end
   
else
    disp('No SDP')
end



%% 1.1.3

for i = 1:n-1
    
    minor_nw = A(1:i, 1:i);
    
    if det(minor_nw) == 0
        disp('Non esiste LU')
        break
    end
    
    if i == n-1
        disp('Esiste LU unica')
    end
    
    
end


luGauss(A)



%% RISOLUZIONE SISTEMA

[L,U,P] = lu(A)

b = [1; 3; 4]
y = fwsub(L,P*b)
x = bksub(U,y)




