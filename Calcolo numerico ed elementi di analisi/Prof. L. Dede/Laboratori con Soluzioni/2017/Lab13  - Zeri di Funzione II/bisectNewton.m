function [ x_vect, k ] = bisectNewton(a,b,itermax_b,itermax_n,tol,f,der_f)

[x_vect1, k1] = bisectForN(a,b,tol,itermax_b,f)

x0 = x_vect1(end);

[x_vect2,k2] = newton(x0,itermax_n,tol,f,der_f);

x_vect = [x_vect1; x_vect2];
k = k1 + k2;


end

