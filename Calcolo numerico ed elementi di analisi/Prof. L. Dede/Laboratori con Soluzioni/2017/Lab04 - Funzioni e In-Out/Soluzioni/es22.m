clc
clear

a(1) = 1;
for ii = 1:10
  a(ii+1) = a(ii)/2 + 1/a(ii);
end
figure,  plot(1:numel(a), a, 'x-b')
hold on,  plot(1:numel(a), sqrt(2)+0*a, 'r--')
