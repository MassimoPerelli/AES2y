clc
clear

k = 8;
vk = zeros(1, k+1);
for ii = 0:k
    vk(ii+1) = (2*ii + 1)^2;
end
vk
