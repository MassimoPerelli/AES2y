function [ x, k ] = richardson( A, b, P, x0, tol, nmax, alpha )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


    k = 0;
    
    x = x0;
    r = b - A*x0;
    err = tol + 1;

    
    
    while (err > tol) & (k < nmax)
        
        k = k+1;

        z = P\r;
        
        if (nargin == 6)
            alpha = ((z')*r)/((z')*A*z);
        end
            
        x_old = x;    
        x = x + alpha*z;
        r = r - alpha*A*z;
        
        err = norm(x-x_old);

        
    end


    

end

