close all

%% 1.1
n = 10;

A = diag(200*ones(n,1)) + diag(32*[0:9]) + diag(-8*ones(n-1,1), 1) + diag(-8*ones(n-1,1), -1) + diag(64*ones(n-2,1), 2) + diag(64*ones(n-2,1), -2);
b = 200*ones(n,1);

if A==A' & min(eig(A))>0
    disp('A è SDP')
end


%% 1.2 
P1 = eye(n);
P2 = diag(diag(A));

if P1==P1' & min(eig(P1))>0
    disp('P1 è SDP')
end

if P2==P2' & min(eig(P2))>0
    disp('P2 è SDP')
end


%% 1.3
% (TH) Siano A,P SDP allora Richardson Stazionario converge per ogni x0 <->
% 0 < α < 2/(λ_max(P^(-1)A))

alpha_max_1 = 2/max(eig(A));
fprintf('\n con P1 convergenza per:\n0 < α < %d\n', alpha_max_1)

P2_inv = diag(1./diag(P2));
alpha_max_2 = 2/max(eig(P2_inv*A));
fprintf('\n con P2 convergenza per:\n0 < α < %d\n', alpha_max_2)
% Si noti che la scelta di P2 con alpha = 1 corrisponde proprio al metodo
% di Jacobi (che converge per ogni x0 essendo alpha = 1 incluso nel range
% 0 < alpha < α_max=1.416765)


%% 1.4.a
alpha_opt1 = 2/(min(eig(A)) + max(eig(A)))
alpha_opt2 = 2/(min(eig(P2_inv*A)) + max(eig(P2_inv*A)))

%% 1.4.b
% B=I-αP⁻¹A
% Per vedere quale dei due precondizionatori da origine al metodo più
% veloce calcolo il raggio spettrale delle matrici di iterazione che si
% ottengono nei due casi, ponendomi per ciascuno di essi in codizioni 
% ottimali, cioè col minimo raggio spettrale possibile (condizione a cui
% corrisponderà naturalmente la scelta del relativo α_opt)

rho_B1 = max(abs(eig( eye(n) - alpha_opt1*A )))
rho_B2 = max(abs(eig( eye(n) - alpha_opt2*P2_inv*A )))


%% 1.5.a
tol = 1e-8;
x0 = zeros(n,1);
nmax = 1000;

[ ~, k1 ] = richardson( A, b, P1, x0, tol, nmax, alpha_opt1 )
[ ~, k2 ] = richardson( A, b, P2, x0, tol, nmax, alpha_opt2 )
fprintf('Verifichiamo empiricamente che effettivamente\nP2 da'' origine al metodo più veloce\nimpiegando %d iterazioni\ncontro le %d iterazioni di P1\n', k1, k2)






