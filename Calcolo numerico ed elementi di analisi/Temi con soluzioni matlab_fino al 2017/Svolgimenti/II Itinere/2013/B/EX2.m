clear all
close all

f = @(t,y) -sin(y);
y0 = pi/2;
t0 = 0; tf = 10;

y_ex = @(t) 2*atan(exp(-t));

t = [t0:0.001:tf];

figure
plot(t, y_ex(t))
grid on
hold on

h_vect = 0.1*2.^-[0 1 2 3];
e_h_vect = [];

for k = 1:length(h_vect)
    
   h = h_vect(k);
   
   [ t_h, u_h ] = Crank_Nicolson( f, y0, t0, tf, h )
   
   
   plot(t_h, u_h)
   
   
   e_h = max(abs(y_ex(t_h)' - u_h));
   e_h_vect = [e_h_vect e_h];


end

legend('y_{ex}(t)', 'y_{h=0.1}', 'y_{h=0.01}', 'y_{h=0.001}')

figure
loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect.^2, '--k')
xlabel('h')
ylabel('err')

legend('e_{h}', 'h^{2}')



