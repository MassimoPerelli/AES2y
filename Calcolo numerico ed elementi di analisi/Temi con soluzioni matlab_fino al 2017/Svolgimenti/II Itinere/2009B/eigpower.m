function [ lambda ] = eigpower( A, x0, tol, itermax)

y = x0/norm(x0);
lambda = y'*A*y;
k = 0;

err = tol+1;

while (err > tol) & (k < itermax)
    k = k+1;
    
    x = A*y;
    y = x/norm(x);
    
    lambda_old = lambda;
    lambda = y'*A*y;
    
    err = abs(lambda - lambda_old)/abs(lambda);
end


return

