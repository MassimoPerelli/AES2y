function [ I ] = trapcomp( f, a, b, M )

H = (b-a)/M;
x = [a:H:b];

I = 0;

for k = 1:M
    I = I + (H/2)*( f(x(k)) + f(x(k+1)));
end


return

