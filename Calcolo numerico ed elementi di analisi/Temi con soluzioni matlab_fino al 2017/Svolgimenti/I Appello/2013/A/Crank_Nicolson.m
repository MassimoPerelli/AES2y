function [t_h,u_h,itFP_vect] = Crank_Nicolson(f,t0,tf, y0, h, tolFP, itermaxFP)



N_h = (tf-t0)/h;

u_h = [y0];
itFP_vect = [];


t_h = [t0:h:tf]';


for n = 0:N_h-1
    
    t_n = n*h;
    t_np1 = (n+1)*h;
    
    u_n = u_h(end);
    
    
    phi = @(u_np1) u_n +  (h/2)*(f(t_n, u_n) + f(t_np1, u_np1) );
    [u_np1, k] = fixedPt(u_n, phi, itermaxFP, tolFP);
    
    u_h = [u_h; u_np1]; 
    
    itFP_vect = [itFP_vect; k];
    
    
end
    
    
    
    return

