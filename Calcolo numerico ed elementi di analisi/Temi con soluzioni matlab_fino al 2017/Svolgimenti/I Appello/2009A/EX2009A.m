%% 1.2
n = 30;
A = diag([2:2:2*n]) + diag([1:n-1], -1) + diag(1./[2:n], 1);
x_exact = ones(n,1);

%Ax=b -> b=A*x;
b=A*x_exact;


%% 1.4
isStrictDiagDomRow = 1;

if det(A)~=0
    
    for i=1:n
        abs_a_ii = abs(A(i,i));
        sumAbsRow = sum(abs(A(i,:)))-abs_a_ii;
        if abs_a_ii <= sumAbsRow
            isStrictDiagDomRow = 0;
        end
    end

    
else
    error('Singular Matrix')
end

if isStrictDiagDomRow
    disp('YES Strict Diagonal Dominance Matrix')
else
    disp('NO Strict Diagonal Dominance Matrix')
end


%% 1.5
x0 = [5:5:150]';
tol = 1e-5;
itermax = 1e+4;
[x, k] = jacobi(A,b,x0,tol,itermax)

e_rel = norm(x-x_exact)/norm(x_exact)
r = b-A*x;
r_rel = norm(r)/norm(b)

cond(A)
